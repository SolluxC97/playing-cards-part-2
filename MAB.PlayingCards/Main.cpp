
// Playing Cards
// Mason Brull
// Izaac Dewilde

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	SPADES,
	DIAMONDS,
	CLUBS,
	HEARTS
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	    case TWO: cout << "The Two of "; break;
		case THREE: cout << "The Three of "; break;
		case FOUR: cout << "The Four of "; break;
		case FIVE: cout << "The Five of "; break;
		case SIX: cout << "The Six of "; break;
		case SEVEN: cout << "The Seven of "; break;
		case EIGHT: cout << "The Eight of "; break;
		case NINE: cout << "The Nine of "; break;
		case TEN: cout << "The Ten of "; break;
		case JACK: cout << "The Jack of "; break;
		case QUEEN: cout << "The Queen of "; break;
		case KING: cout << "The King of "; break;
		case ACE: cout << "The Ace of "; break;
	}

	switch (card.Suit)
	{
	    case SPADES: cout << "Spades \n"; break;
		case DIAMONDS: cout << "Diamonds \n"; break;
		case CLUBS: cout << "Clubs \n"; break;
		case HEARTS: cout << "Hearts \n"; break;
	}
}

Card HighCard(Card c1, Card c2)
{
	if (c1.Rank > c2.Rank)
	{
		return c1;
	}
	return c2;
}

int main()
{
	Card c1;
	c1.Rank = Rank::KING;
	c1.Suit = Suit::CLUBS;

	Card c2;
	c2.Rank = Rank::QUEEN;
	c2.Suit = Suit::SPADES;

	cout << "The card with the highest rank is ";
	PrintCard(HighCard(c1, c2));

	(void)_getch();
	return 0;
}
